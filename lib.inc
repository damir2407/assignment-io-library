section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .count:
    	cmp byte [rdi+rax], 0
    	je .end
    	inc rax
    	jmp .count
    .end:
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall 
    .end:
    	ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 
    push 0xA
    mov rax, rdi
    mov r12, 10
    .div_cycle:
        xor rdx, rdx
        div r12
        add rdx, '0'
        push rdx
        cmp rax, 0
        je .char_cycle
        jmp .div_cycle
        pop rdx
    .char_cycle:
        pop rdi
        cmp rdi, 0xA
        je .end
        call print_char
        jmp .char_cycle
    .end:
        ret         


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
   .if_pos:
    	cmp rdi, 0
    	jns .uint_mark
   .if_neg:
    	push rdi
    	mov rdi, '-'
    	call print_char
    	pop rdi
    	neg rdi
    .uint_mark:
        call print_uint
    .end:
    	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r10, r10
    xor r8, r8
  .loop:
    	mov r10b, [rdi+r8]
    	mov r11b, [rsi+r8]
    	cmp r10b, r11b
    	jne .not_equals
    	cmp r10b, 0
    	je .equals
    	inc r8
    	jmp .loop
   .not_equals:
    	xor rax, rax
    	ret
   .equals:
    	mov rax, 1 
    	ret
   

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    .end:
    	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi
    push rdi
    push rsi
    
    .begin_space_skip:
    	call read_char
    	cmp rax, 0x20
    	je .begin_space_skip
    	cmp rax, 0x9
    	je .begin_space_skip
    	cmp rax, 0xA
    	je .begin_space_skip
    	xor r9,r9
    	pop rsi
    	pop rdi
    
    .read_char_cycle:
    	cmp rax, 0
    	je .success
    	cmp rax, 0x20
    	je .success
    	cmp rax, 0x9
    	je .success
    	cmp rax, 0xA
    	je .success
    	cmp rsi, r9
    	je .fail
    	mov [rdi], rax
    	inc rdi
    	inc r9
    	push rdi
    	push rsi
    	call read_char
    	pop rsi
    	pop rdi
    	jmp .read_char_cycle
    
    .success:
    	pop rax
    	mov rdx, r9
    	ret
    
    .fail:
    	pop rax
    	xor rax, rax
    	ret
 

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint: 
    xor rax, rax
    xor r11, r11
    mov rsi, 10
    xor r8, r8
.int_cycle:
    mov r11b, byte [rdi + r8]
    cmp r11b, 0x30
    jl .end
    cmp r11b, 0x39
    jg .end
    mul rsi
    inc r8
    sub r11b, 0x30
    add rax, r11
    jmp .int_cycle
.end:
    mov rdx, r8
    ret

   




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
.check:
    cmp byte[rdi],0x2d
    je .negative_cycle
.positive_cycle:
    call parse_uint
    ret
.negative_cycle:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    .check_on_good_length:
    	call string_length
    	cmp rdx, rax 
    	jb .end
    .copy_cycle:
        xor r11, r11
        mov r11b, byte[rdi]
        mov byte[rsi], r11b
        cmp r11, 0
        je .end
        inc rsi
        inc rdi
        jmp .copy_cycle
    .end:
        xor rax, rax
        ret
